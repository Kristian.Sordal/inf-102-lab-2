package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	public static final int DEFAULT_CAPACITY = 10;

	private int n;
	private int length;

	private Object elements[];

	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}

	@Override
	public T get(int index) {
		if (index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException();
		}

		if (isEmpty()) {
			return null;
		}

		// --- typecasts the object to a generic type T
		return (T) elements[index];
	}

	@Override
	public void add(int index, T element) {
		int size = size();

		// --- if n is 0, simply add the element
		if (n == 0) {
			elements[n] = element;
			n++;
			return;
		}

		/*  --- if index of added element is equal to lenght - 1 (aka it fills up the last spot),
		 	--- it can simply be added, as no other values need to be moved.
		
			--- if the index is smaller than length -1, and the arrays size is smaller than its length
			--- the elements of the array is moved to the index ahead, and the element to be added
			--- is added at its respective index.

			--- else, the case is that the arraylist needs to be resized. (there might be an edgecase where this case fails)
			--- but so far it seems to hold up.
		*/
		if (index == length - 1) {
			elements[size] = element;
			n++;
		} else if (index < length - 1 && size < length){
			Object tempArr[] = elements.clone();

			for (int i = index; i < size; i++) {
				tempArr[i + 1] = elements[i];
			}

			tempArr[index] = element;
			elements = tempArr.clone();
			n++;
		} else {
			Object tempArr[] = new Object[size * 2];
			length = size * 2;

			for (int i = 0; i < size; i++) {
				tempArr[i] = elements[i];
			}

			tempArr[index] = element;
			elements = tempArr.clone();
			n++;
		}
	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n * 3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n - 1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}
}